# Multiplayer Game Prototype

## Requirements

- Unity (v2022.3.20f1)
- Node.js (v16.20.2)
- npm (v8.19.4)

## Download

1. Clone the repository:
   ```sh
   git clone https://gitlab.com/krystianwludarczyk/multiplayer-game-prototype
   ```
2. Navigate to the project directory:

   ```sh
   cd multiplayer-game-prototype
   ```

## Installation - Server

1. Navigate to the server directory:

   ```sh
   cd server
   ```

2. Install dependencies for server:

   ```sh
   npm install
   ```

3. Run server:
   ```sh
   node server.js
   ```
