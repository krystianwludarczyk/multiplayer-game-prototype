const WebSocket = require("ws");
const { service } = require("./service");
const wss = new WebSocket.Server({ port: 8080 });

wss.on("connection", (ws) => {
  ws.on("message", (message) => {
    const data = JSON.parse(message.toString("utf8"));

    //console.log(message.toString("utf8"));

    switch (data.eventType) {
      case "try_createRoom":
        service.createRoom(ws, data.username);
        break;
      case "try_joinRoom":
        service.joinRoom(data.roomId, ws, data.username);
        break;
      case "try_updatePosition":
        service.updatePosition(
          data.roomId,
          data.id,
          data.x,
          data.y,
          data.facingRight
        );
        break;
      default:
        break;
    }
  });

  ws.on("close", () => {
    service.leaveRoom(ws);
  });
});

console.log("WebSocket server is running on ws://localhost:8080");
