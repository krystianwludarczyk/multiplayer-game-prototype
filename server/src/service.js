const Room = require("./entity/room.js");
const User = require("./entity/user.js");

const service = {
  roomIdCounter: 0,
  rooms: {},

  GetUniqueRoomId() {
    const id = this.roomIdCounter;
    this.roomIdCounter += 1;
    return id;
  },

  createRoom(client, username) {
    const newRoomId = this.GetUniqueRoomId();
    this.rooms[newRoomId] = new Room(newRoomId);

    const newUserId = this.rooms[newRoomId].GetUniqueUserId();
    const newUser = new User(client, newUserId, username);
    this.rooms[newRoomId].users.push(newUser);

    newUser.client.send(
      JSON.stringify({
        eventType: "source_joinRoomS",
        roomId: newRoomId,
        users: this.rooms[newRoomId].users.map((u) => ({
          id: u.id,
          username: u.username,
        })),
        asUser: {
          id: newUser.id,
          username: newUser.username,
        },
      })
    );
  },

  joinRoom(roomId, client, username) {
    if (!this.rooms[roomId]) return false;

    const newUser = new User(
      client,
      this.rooms[roomId].GetUniqueUserId(),
      username
    );

    this.rooms[roomId].users.forEach((user) => {
      user.client.send(
        JSON.stringify({
          eventType: "other_joinRoomS",
          user: {
            id: newUser.id,
            username: newUser.username,
          },
        })
      );
    });

    newUser.client.send(
      JSON.stringify({
        eventType: "source_joinRoomS",
        roomId,
        users: this.rooms[roomId].users.map((u) => ({
          id: u.id,
          username: u.username,
        })),
        asUser: {
          id: newUser.id,
          username: newUser.username,
        },
      })
    );

    this.rooms[roomId].users.push(newUser);

    return true;
  },

  leaveRoom(client) {
    for (const roomId in this.rooms) {
      if (this.rooms.hasOwnProperty(roomId)) {
        const room = this.rooms[roomId];
        const userWhoLeftIndex = room.users.findIndex(
          (user) => user.client === client
        );
        if (userWhoLeftIndex !== -1) {
          const userWhoLeft = room.users[userWhoLeftIndex];
          room.users.splice(userWhoLeftIndex, 1);

          if (room.users.length === 0) {
            delete this.rooms[roomId];
            console.log(`Room ${roomId} deleted because it is empty`);
          } else {
            room.users.forEach((user) => {
              user.client.send(
                JSON.stringify({
                  eventType: "other_leftRoomS",
                  user: {
                    id: userWhoLeft.id,
                    username: userWhoLeft.username,
                  },
                })
              );
            });
          }
        }
      }
    }
    return true;
  },

  updatePosition(roomId, id, x, y, facingRight) {
    if (this.rooms[roomId]) {
      this.rooms[roomId].users.forEach((user) => {
        if (user.id !== id) {
          user.client.send(
            JSON.stringify({
              eventType: "other_updatePositionS",
              user: {
                id,
                x,
                y,
                facingRight,
              },
            })
          );
        }
      });
      return true;
    }
    return false;
  },
};

module.exports = { service };
