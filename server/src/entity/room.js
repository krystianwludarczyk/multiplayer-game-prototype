class Room {
  constructor(roomId) {
    this.roomId = roomId;
    this.userIdCouter = 1;
    this.users = [];
  }

  GetUniqueUserId() {
    const id = this.userIdCouter;
    this.userIdCouter += 1;
    return id;
  }
}
module.exports = Room;
