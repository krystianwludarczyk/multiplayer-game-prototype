class User {
  constructor(client, id, username) {
    this.id = id;
    this.client = client;
    this.username = username;
  }
}

module.exports = User;
