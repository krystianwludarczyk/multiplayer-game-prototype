[System.Serializable]
public class User
{
    public int id;
    public string username;
    public float x, y;
    public bool facingRight;
}