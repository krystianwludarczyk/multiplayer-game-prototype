using System.Collections.Generic;
using NativeWebSocket;
using UnityEngine;
using UnityEngine.UI;

public class WebHandler : MonoBehaviour
{
    WebSocket ws;

    [Header("Players")]
    [SerializeField] GameObject localPlayerPrefab;
    [SerializeField] GameObject fakePlayerPrefab;

    LocalPlayer localPlayer = null;
    List<FakePlayer> fakePlayers = new List<FakePlayer>();

    [Header("UI Elements")]
    [SerializeField] GameObject startMenuUI;
    [SerializeField] GameObject gameMenuUI;
    [SerializeField] InputField usernameInput;
    [SerializeField] InputField roomIdInput;

    [Header("UI Elements")]
    [SerializeField] Text debugText;

    void Start()
    {
        ws = new WebSocket("ws://127.0.0.1:8080");
        ws.OnOpen += () => { Debug.Log("WebSocket connection open!"); };

        ws.OnMessage += (bytes) =>
        {
            string message = System.Text.Encoding.UTF8.GetString(bytes);
            HandleInput(message);
        };

        ws.Connect();
    }

    void HandleInput(string message)
    {
        //Debug.Log(message);
        InputMessage inputMessage = JsonUtility.FromJson<InputMessage>(message);
        switch (inputMessage.eventType)
        {
            case "source_joinRoomS":
                source_joinRoomS source_joinRoomS_message = JsonUtility.FromJson<source_joinRoomS>(message);

                InstantiatePlayer(localPlayerPrefab, source_joinRoomS_message.asUser.id, source_joinRoomS_message.asUser.username);
                localPlayer.currentRoomId = source_joinRoomS_message.roomId;

                for (int i = 0; i < source_joinRoomS_message.users.Length; i++)
                {
                    if (source_joinRoomS_message.users[i].id != localPlayer.id)
                    {
                        InstantiatePlayer(fakePlayerPrefab, source_joinRoomS_message.users[i].id, source_joinRoomS_message.users[i].username);
                    }
                }

                startMenuUI.SetActive(false);
                gameMenuUI.SetActive(true);
                InvokeRepeating(nameof(RefreshDebug), 1f, 0.5f);
                break;
            case "other_joinRoomS":
                other_joinRoomS other_joinRoomS_message = JsonUtility.FromJson<other_joinRoomS>(message);

                InstantiatePlayer(fakePlayerPrefab, other_joinRoomS_message.user.id, other_joinRoomS_message.user.username);
                break;
            case "other_leftRoomS":
                other_leftRoomS other_leftRoomS_message = JsonUtility.FromJson<other_leftRoomS>(message);

                RemovePlayer(other_leftRoomS_message.user.username);
                break;
            case "other_updatePositionS":
                other_updatePositionS other_updatePositionS_message = JsonUtility.FromJson<other_updatePositionS>(message);
                UpdatePlayerPosition(other_updatePositionS_message.user.id, other_updatePositionS_message.user.x, other_updatePositionS_message.user.y, other_updatePositionS_message.user.facingRight);
                break;
            default:
                break;
        }


    }

    void RefreshDebug()
    {
        debugText.text = "";
        debugText.text += "Room ID: " + localPlayer.currentRoomId + "\n";
        debugText.text += "PLAYERS: " + "\n";
        for (int i = 0; i < fakePlayers.Count; i++)
        {
            debugText.text += "[ <" + fakePlayers[i].id + "> " + fakePlayers[i].username + " | (" + fakePlayers[i].GetRealPosition().x + "," + fakePlayers[i].GetRealPosition().y + ") ]" + "\n";
        }
    }

    void InstantiatePlayer(GameObject playerPrefab, int id, string username)
    {
        GameObject newPlayer = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
        if (playerPrefab == localPlayerPrefab)
        {
            newPlayer.GetComponent<LocalPlayer>().id = id;
            newPlayer.GetComponent<LocalPlayer>().username = username;
            localPlayer = newPlayer.GetComponent<LocalPlayer>();
        }
        else
        {
            newPlayer.GetComponent<FakePlayer>().id = id;
            newPlayer.GetComponent<FakePlayer>().username = username;
            fakePlayers.Add(newPlayer.GetComponent<FakePlayer>());
        }
    }

    void RemovePlayer(string username)
    {
        FakePlayer playerToRemove = fakePlayers.Find(player => player.username == username);
        if (playerToRemove != null)
        {
            fakePlayers.Remove(playerToRemove);
            Destroy(playerToRemove.gameObject);
        }
    }

    void UpdatePlayerPosition(int id, float x, float y, bool facingRight)
    {
        foreach (FakePlayer player in fakePlayers)
        {
            if (player.id != id)
            {
                continue;
            }

            player.SetRealPosition(new Vector2(x, y), facingRight);
            break;
        }
    }

    public void CreateRoom()
    {
        string myUsername = usernameInput.text;
        string json = "{\"eventType\": \"try_createRoom\", \"username\": \"" + myUsername + "\"}";
        ws.SendText(json);
    }

    public void JoinRoom()
    {
        string myUsername = usernameInput.text;
        string json = "{\"eventType\": \"try_joinRoom\", \"username\": \"" + myUsername + "\", \"roomId\": " + roomIdInput.text + "}";
        ws.SendText(json);
    }

    public void SendPosition(float x, float y, bool facingRight)
    {
        string json = "{\"eventType\": \"try_updatePosition\", \"id\": " + localPlayer.id + ", \"roomId\": " + localPlayer.currentRoomId + ", \"x\": " + x.ToString("F2", System.Globalization.CultureInfo.InvariantCulture) + ", \"y\": " + y.ToString("F2", System.Globalization.CultureInfo.InvariantCulture) + ", \"facingRight\": " + facingRight.ToString().ToLower() + "}";
        ws.SendText(json);
    }

    void OnDestroy()
    {
        ws.Close();
    }

    void Update()
    {
#if !UNITY_WEBGL || UNITY_EDITOR
        ws.DispatchMessageQueue();
#endif
    }
}
