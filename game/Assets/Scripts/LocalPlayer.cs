using UnityEngine;

public class LocalPlayer : MonoBehaviour
{
    WebHandler webHandler;

    public int currentRoomId;
    public int id;
    public string username;

    readonly float moveSpeed = 4f;
    bool facingRight = true;

    SpriteRenderer spriteRenderer;
    Rigidbody2D rb2D;

    void Start()
    {
        webHandler = FindObjectOfType<WebHandler>();
        InvokeRepeating(nameof(SendMyPos), 0.1f, 0.1f);
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");

        if (moveHorizontal > 0)
            spriteRenderer.flipX = false;
        else if (moveHorizontal < 0)
            spriteRenderer.flipX = true;

        facingRight = !spriteRenderer.flipX;
        Vector2 movementDirection;
        movementDirection = new Vector2(moveHorizontal, moveVertical);
        movementDirection = Vector2.ClampMagnitude(movementDirection, 1f);
        rb2D.velocity = movementDirection * moveSpeed;
    }

    void SendMyPos()
    {
        webHandler.SendPosition(transform.position.x, transform.position.y, facingRight);
    }
}
