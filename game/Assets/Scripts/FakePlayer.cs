using UnityEngine;

public class FakePlayer : MonoBehaviour
{
    public int id;
    public string username;
    Vector2 realPosition;

    void Update()
    {
        transform.position = Vector2.Lerp(transform.position, realPosition, Time.deltaTime * 10f);
    }
    public Vector2 GetRealPosition()
    {
        return realPosition;
    }

    public void SetRealPosition(Vector2 newPosition, bool facingRight)
    {
        realPosition = newPosition;
        transform.localRotation = Quaternion.Euler(0, facingRight ? 0 : 180, 0);
    }
}
